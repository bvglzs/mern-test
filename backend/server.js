const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const cors = require('cors');
const mongoose = require('mongoose');
const PORT = 4000;
var coursesRouter = require('./routes/courses.route');

app.use(cors());
app.use(bodyParser.json());

const uriDatabase = 'mongodb://bvglzs:bvglzs10@ds333238.mlab.com:33238/courses'

mongoose.connect(uriDatabase).then(
  () => { console.log('Database connection is successful') },
  err => { console.log('Error when connecting to the database' + err) });


mongoose.connect('mongodb://127.0.0.1:27017/courses', { useNewUrlParser: true });
const connection = mongoose.connection;

connection.once('open', () => {
    console.log('MongoDB database connection established successfully');
})

app.use('/courses', coursesRouter);

app.listen(PORT, () => {
    console.log("Server is running on PORT: " + PORT);
});