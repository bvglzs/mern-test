var express = require('express');
const paginate = require('jw-paginate');
var Courses = require('../models/courses.model');
var router = express.Router();

router.route('/getCoursesList').get(
    (req, res) => {
        Courses.find((err, courses) => {
            if (err) {
                console.log(err);
            } else {
                const page = parseInt(req.query.page) || 1;
                const pageSize = 6;
                const pager = paginate(courses.length, page, pageSize);
                const pageOfItems = courses.slice(pager.startIndex, pager.endIndex + 1);
                res.json({ pager, pageOfItems });
            }
        });
    }
);

router.route('/createCourse').post(
    (req, res) => {
        let course = new Courses(req.body);
        course.save().then(
            course => {
                res.status(200).json({'course': 'course added successfully'});
            }
        )
        .catch( err => {
            res.status(400).send('adding new course failed');
        });
    }
);

module.exports = router;