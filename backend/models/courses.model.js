const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let Courses = new Schema({
    course_id: {
        type: Number
    },
    course_img: {
        type: String
    },
    course_name: {
        type: String
    },
    course_description: {
        type: String
    },
    course_price: {
        type: Number
    },
    course_total_votes: {
        type: Number
    },
    course_total_people_vote: {
        type: Number
    }    
});

module.exports = mongoose.model('Courses', Courses);