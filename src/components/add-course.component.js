import React from 'react';
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import Grid from "@material-ui/core/Grid";
import axios from 'axios';
import TextField from "@material-ui/core/TextField";
import PropTypes from 'prop-types';
import NumberFormat from 'react-number-format';
import Button from "@material-ui/core/Button";

export default function AddCourse(props) {
    const { closeModal, getDataAfterCreate } = props;
    const [courseName, setCourseName] = React.useState("");
    const [description, setDescription] = React.useState("");
    const [img, setImg] = React.useState("");
    const [totalVotes, setTotalVotes] = React.useState("");
    const [personVote, setPersonVote] = React.useState("");
    const [values, setValues] = React.useState({
        numberformat: '',
    });
    const handleChangePrice = name => event => {
        setValues({
            ...values,
            [name]: event.target.value,
        });
    };

    const handleChangeCourseName = event => {
        setCourseName(event.target.value);
    }

    const handleChangeDescription = event => {
        setDescription(event.target.value);
    }

    const handleChangeImg = event => {
        setImg(event.target.value);
    }

    const handleChangeTotalVotes = event => {
        setTotalVotes(event.target.value);
    }

    const handleChangePersonVote = event => {
        setPersonVote(event.target.value);
    }
    return (

        <div style={{ width: 600, borderRadius: 40 }}>
            <DialogTitle
                id="scroll-dialog-title"
                style={{
                    textAlign: "center",
                    color: "#00ADEE",
                    fontSize: 30
                }}
            >
                Create new course
            </DialogTitle>
            <DialogContent>
                <DialogContentText>
                    <Grid item md={12} xs={12}>
                        <TextField
                            id="standard-basic"
                            label="Please insert course name"
                            style={{ width: "100%" }}
                            onChange={handleChangeCourseName}
                            value={courseName}
                        />
                    </Grid>
                    <Grid item md={12} xs={12}>
                        <TextField
                            id="standard-basic"
                            label="Please insert course description"
                            style={{ width: "100%" }}
                            onChange={handleChangeDescription}
                            value={description}
                        />
                    </Grid>
                    <Grid item md={12} xs={12}>
                        <TextField
                            id="standard-basic"
                            label="Please insert image url"
                            style={{ width: "100%" }}
                            onChange={handleChangeImg}
                            value={img}
                        />
                    </Grid>
                    <Grid item md={12} xs={12}>
                        <TextField
                            id="formatted-numberformat-input"
                            label="Please insert course price"
                            onChange={handleChangePrice('numberformat')}
                            InputProps={{
                                inputComponent: NumberFormatCustom,
                            }}
                            style={{ width: "100%" }}
                            value={values.numberformat}
                        />
                    </Grid>
                    <Grid item md={12} xs={12}>
                        <TextField
                            id="standard-basic"
                            label="Please insert total votes for this course"
                            style={{ width: "100%" }}
                            type="number"
                            onChange={handleChangeTotalVotes}
                            value={totalVotes}
                        />
                    </Grid>
                    <Grid item md={12} xs={12}>
                        <TextField
                            id="standard-basic"
                            label="Please insert how many person was vote"
                            style={{ width: "100%" }}
                            type="number"
                            onChange={handleChangePersonVote}
                            value={personVote}
                        />
                    </Grid>
                </DialogContentText>
            </DialogContent>
            <DialogActions>
                <Button color="primary" onClick={async () => {
                    closeModal()
                }}>
                    Volver
            </Button>
                <Button onClick={async () => {  
                    if(courseName === "" || description === "" || values.numberformat === '' || totalVotes === "" ||
                        personVote === "") 
                        {
                            alert('All fields are require');
                            return;  
                        }    
                    const imgSave = img===""?"https://banner2.cleanpng.com/20180505/cre/kisspng-book-cover-outline-clip-art-5aed58b656e528.2169014815255041823559.jpg":img;           
                    const data = {
                        'course_name': courseName,
                        'course_img': imgSave,
                        'course_description': description,
                        'course_price': parseFloat(values.numberformat),
                        'course_total_votes': totalVotes,
                        'course_total_people_vote': personVote
                    }
                    console.log(data)
                    axios.post('http://localhost:4000/courses/createCourse',data)
                            .then(response => {
                                getDataAfterCreate();
                            })
                            .catch(function (error) {
                                console.log(error);
                            });
                }} color="primary">
                    Crear
            </Button>
            </DialogActions>
        </div>
    )
}

function NumberFormatCustom(props) {
    const { inputRef, onChange, ...other } = props;

    return (
        <NumberFormat
            {...other}
            getInputRef={inputRef}
            onValueChange={values => {
                onChange({
                    target: {
                        value: values.value,
                    },
                });
            }}
            thousandSeparator
            isNumericString
            prefix="$"
        />
    );
}

NumberFormatCustom.propTypes = {
    inputRef: PropTypes.func.isRequired,
    onChange: PropTypes.func.isRequired,
};