import React from 'react';
import axios from 'axios';
import CourseCard from './course-card.component';
import AddCourse from './add-course.component';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import IconButton from "@material-ui/core/IconButton";
import AddIcon from '@material-ui/icons/Add';
import Dialog from "@material-ui/core/Dialog";
import { Link } from 'react-router-dom';


class CoursesList extends React.Component {

    constructor(props){
        super(props);
        this.state = {
            courses: [],
            coursesFilter: [],
            showAddCourseModal: false,
            urlCourseList: 'http://localhost:4000/courses/getCoursesList',
            pager: {},
            pageOfItems: []
        };
    }

    componentDidMount(){        
        this.loadPage();
    }
    
    componentDidUpdate() {
        this.loadPage();
    }

    searchCourses = event => {
        const valueToSearch = event.target.value.toLowerCase();
        const coursesFiltered = this.state.pageOfItems.filter(course => course.course_name.toLowerCase().includes(valueToSearch) || 
                                                            course.course_description.toLowerCase().includes(valueToSearch));
        this.setState({
            coursesFilter: coursesFiltered
        });
    }

    getDataAfterCreate = () => {
        axios.get(this.state.urlCourseList)
            .then(response => {
                this.closeModal();
            }
            )
            .catch(function (error) {
                console.log(error);
            })
    }

    openModal = () => {
        this.setState({
            showAddCourseModal:true
        })
    }

    closeModal = () => {
        this.setState({
            showAddCourseModal:false
        })
        this.loadPage();
    }

    loadPage() {
        // get page of items from api
        const params = new URLSearchParams(this.props.location.search);
        const page = parseInt(params.get('page')) || 1;
        if (page !== this.state.pager.currentPage) {
            fetch(`${this.state.urlCourseList}?page=${page}`, { method: 'GET' })
                .then(response => response.json())
                .then(({pager, pageOfItems}) => {
                    this.setState({ pager, pageOfItems, coursesFilter: pageOfItems });
                });
        }
    }

    render() {
        const { showAddCourseModal, coursesFilter, pager } = this.state;
        return ( 
            <div style={{padding: 20, backgroundColor: "#e6f0ff"}}>
                <Toolbar position="static" style={{ backgroundColor: "#1a75ff" }}>
                    <Typography variant="h3" color="inherit" style={{ color: "white" }}>
                       Courses List
                    </Typography>
                    <span style={{marginLeft: "auto",marginRight: -12}}>
                        <IconButton color="inherit" aria-label="More Options" style={{ color: "white" }}>
                            <AddIcon onClick={this.openModal}/>
                        </IconButton>
                    </span>
                </Toolbar>
                <hr/><hr/>
                <TextField
                    onChange={this.searchCourses}
                    placeholder="Search courses by title or description"
                    InputProps={{
                        style: {
                        justifyContent: "center",
                        textAlign: "center",
                        height: 50,
                        borderWidth: 2,
                        borderColor: "#00adee",
                        borderStyle: "solid",
                        borderRadius: 20,
                        backgroundColor: "#FFFFFF",
                        backgroundSize: "20px 20px",
                        paddingLeft: 25,
                        backgroundPosition: "8px 13px",
                        ":focus": {
                            border: "0px"
                        }
                        }
                    }}
                    fullWidth
                    />
                    
                <hr/><hr/>
                <Grid container spacing={3}>
                    {coursesFilter.map((course, index) => (
                            <CourseCard img={course.course_img} name={course.course_name} description={course.course_description} 
                                        price={course.course_price} total_votes={course.course_total_votes} total_person_vote={course.course_total_people_vote}/>
                
                    ))}
                    
                    <Dialog open={showAddCourseModal} scroll={"body"}>
                        <AddCourse closeModal={this.closeModal} getDataAfterCreate={this.getDataAfterCreate}/>
                        
                    </Dialog>
                </Grid>
                <div className="card-footer pb-0 pt-3">
                    {pager.pages && pager.pages.length &&
                        <ul className="pagination">
                            <li className={`page-item previous-item ${pager.currentPage === 1 ? 'disabled' : ''}`}>
                                <Link to={{ search: `?page=${pager.currentPage - 1}` }} className="page-link">Previous</Link>
                            </li>
                            {pager.pages.map(page =>
                                <li key={page} className={`page-item number-item ${pager.currentPage === page ? 'active' : ''}`}>
                                    <Link to={{ search: `?page=${page}` }} className="page-link">{page}</Link>
                                </li>
                            )}
                            <li className={`page-item next-item ${pager.currentPage === pager.totalPages ? 'disabled' : ''}`}>
                                <Link to={{ search: `?page=${pager.currentPage + 1}` }} className="page-link">Next</Link>
                            </li>
                        </ul>
                    }                    
                </div>
            </div>
        )
    }
}

export default CoursesList;