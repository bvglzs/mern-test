import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import Rating from '@material-ui/lab/Rating';

const useStyles = makeStyles(theme => ({
    root: {
      flexGrow: 1,
      marginBottom: 5,
      marginTop: 10,
    },
    paper: {
      padding: theme.spacing(2),
      margin: 'auto',
      maxWidth: 500
    },
    image: {
      width: 128,
      height: 128,
    },
    img: {
      margin: 'auto',
      display: 'block',
      Width: '100px',
      maxHeight: '100px',
    },
  }));

export default function CourseCard (props){
    const { img, name, description, price, total_votes, total_person_vote} = props;
    
    const classes = useStyles();
    return(
        <Grid item xs={4} className={classes.root}>
            <Paper className={classes.paper} style={{ backgroundColor: "#f2f2f2"}}>
                <Grid item style={{ position: "relative", left: "-20%", paddingBottom: 20 }}>
                    <img className={classes.img} alt="complex" src={img!==""?img:"https://banner2.cleanpng.com/20180505/cre/kisspng-book-cover-outline-clip-art-5aed58b656e528.2169014815255041823559.jpg"} />
                </Grid>
                <Grid item xs={12} sm container>
                    <Grid item xs container direction="column" spacing={2}>
                        <Grid item xs>
                            <Typography gutterBottom variant="h6">
                                <b>{name}</b>
                            </Typography>
                            <Typography variant="p" style={{fontFamily:'italic', fontSize: 15}} gutterBottom>
                                {description}
                            </Typography>
                        </Grid>
                    </Grid>
                </Grid>
                <br></br>
                <Grid item xs={12} sm container spacing={2}>
                    <Grid item>
                        <Typography variant="body2" >
                            {price===0.0?"Free":"$"+price}
                        </Typography>
                    </Grid>
                    <Grid item style={{ position: "relative", right: -100 }}>
                        <Rating name="read-only" value={total_votes/total_person_vote} readOnly />
                    </Grid>
                </Grid>
            </Paper>
        </Grid>
    )
}

