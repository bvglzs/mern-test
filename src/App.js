import React from 'react';
import { BrowserRouter as Router, Route} from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";

import CoursesList from "./components/courses-list.component"

class App extends React.Component{
  render(){
    return (
      <Router>
        <div style={{ backgroundColor: "#e6f0ff"}}>
          <div className="container">
            <Route path="/" exact component={CoursesList}/>
          </div>
        </div>
        
      </Router>
      );
  }
}

export default App;
